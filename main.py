import os
import re

import nltk as nltk

vocabulary = {}


def list_filepaths(folder):
    # consultez la documentation des modules os et os.path pour vous aider
    return []  # TODO 1: lire le contenu du dossier <folder> et retourner une liste de chemins relatifs


def load_text(filepath):
    raw = open(filepath, 'r', encoding='utf8').read()
    return re.sub(r'^[\S\s]+[*]{3} START OF .+[*]{3}([\S\s]+)[*]{3} END OF .*[*]{3}[\S\s]+$', r'\1', raw)


def preprocess(text):
    # nltk.download('punkt')
    # nltk.download('stopwords')
    #TEST
    stopwords = set(nltk.corpus.stopwords.words('french'))
    stemmer = nltk.stem.snowball.FrenchStemmer()
    tokens = []
    for line in text.strip().split('\n\n'):
        if not re.match(r'\s+', line):
            tokens.extend([stemmer.stem(t) for t in nltk.word_tokenize(line.strip().lower(), "french")
                           if t.isalpha() and t not in stopwords])
    return tokens


def extract_ngrams(tokens, n=1):
    ngrams = {}
    # TODO 2: parcourir la liste <tokens> et remplir le dictionnaire <ngrams> associant chaque n-gramme avec sa fréquence
    return ngrams


def fill_vocabulary(ngrams):
    pass  # TODO 3: ajouter les éléments de <ngrams> à <vocabulary> ; attention à bien additionner les fréquences


def plot_frequency_distribution(tokens):
    fdist = nltk.probability.FreqDist(tokens)
    fdist.plot(50, cumulative=True)


if __name__ == '__main__':
    for filepath in list_filepaths('data'):
        tokens = preprocess(load_text(filepath))
        ngrams = extract_ngrams(tokens, 2)
        fill_vocabulary(ngrams)
    plot_frequency_distribution(vocabulary)
